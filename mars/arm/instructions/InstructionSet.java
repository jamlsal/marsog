package mars.arm.instructions;

import mars.simulator.*;
import mars.util.Binary;
import mars.arm.hardware.*;
import mars.arm.instructions.syscalls.*;
import mars.*;

import java.util.*;
import java.io.*;

/*
Copyright (c) 2003-2013,  Pete Sanderson and Kenneth Vollmar

Developed by Pete Sanderson (psanderson@otterbein.edu)
and Kenneth Vollmar (kenvollmar@missouristate.edu)

Permission is hereby granted, free of charge, to any person obtaining 
a copy of this software and associated documentation files (the 
"Software"), to deal in the Software without restriction, including 
without limitation the rights to use, copy, modify, merge, publish, 
distribute, sublicense, and/or sell copies of the Software, and to 
permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be 
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR 
ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(MIT license, http://www.opensource.org/licenses/mit-license.html)
*/

/**
 * The list of Instruction objects, each of which represents a MIPS instruction.
 * The instruction may either be basic (translates into binary machine code) or
 * extended (translates into sequence of one or more basic instructions).
 *
 * @author Pete Sanderson and Ken Vollmar
 * @version August 2003-5
 */

public class InstructionSet {
	private ArrayList instructionList;
	private ArrayList opcodeMatchMaps;
	private SyscallLoader syscallLoader;

	/**
	 * Creates a new InstructionSet object.
	 */
	public InstructionSet() {
		instructionList = new ArrayList();

	}

	/**
	 * Retrieve the current instruction set.
	 */
	public ArrayList getInstructionList() {
		return instructionList;

	}

	/**
	 * Adds all instructions to the set. A given extended instruction may have more
	 * than one Instruction object, depending on how many formats it can have.
	 * 
	 * @see Instruction
	 * @see BasicInstruction
	 * @see ExtendedInstruction
	 */
	public void populate() {
		/*
		 * Here is where the parade begins. Every instruction is added to the set here.
		 */

		// //////////////////////////////////// BASIC INSTRUCTIONS START HERE
		// ////////////////////////////////

		instructionList.add(new BasicInstruction("ADD X1,X2,X3", "Add : set X1 to (X2 plus X3)",
				BasicInstructionFormat.R_FORMAT, "000000 sssss ttttt fffff 00000 100000", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						long add1 = RegisterFile.getValue(operands[1]);
						long add2 = RegisterFile.getValue(operands[2]);
						long sum = add1 + add2;
						// overflow on A+B detected when A and B have same sign and A+B has other sign.
						if ((add1 >= 0 && add2 >= 0 && sum < 0) || (add1 < 0 && add2 < 0 && sum >= 0)) {
							throw new ProcessingException(statement, "arithmetic overflow",
									Exceptions.ARITHMETIC_OVERFLOW_EXCEPTION);
						}
						RegisterFile.updateRegister(operands[0], sum);
					}
				}));
		instructionList.add(
				new BasicInstruction("ADDI X1,X2,-100", "Add Immediate : set X1 to (X2 plus signed 16-bit immediate)",
						BasicInstructionFormat.I_FORMAT, "001000 sssss fffff tttttttttttttttt", new SimulationCode() {
							public void simulate(ProgramStatement statement) throws ProcessingException {
								long[] operands = statement.getOperands();
								long add1 = RegisterFile.getValue(operands[1]);
								long add2 = operands[2] << 16 >> 16;
								long sum = add1 + add2;
								// overflow on A+B detected when A and B have same sign and A+B has other sign.
								if ((add1 >= 0 && add2 >= 0 && sum < 0) || (add1 < 0 && add2 < 0 && sum >= 0)) {
									throw new ProcessingException(statement, "arithmetic overflow",
											Exceptions.ARITHMETIC_OVERFLOW_EXCEPTION);
								}
								RegisterFile.updateRegister(operands[0], sum);
							}
						}));
		instructionList.add(new BasicInstruction("ADDIS X1,X2,-100",
				"Add Immediate : set X1 to (X2 plus signed 16-bit immediate) and set flags",
				BasicInstructionFormat.I_FORMAT, "001000 sssss fffff tttttttttttttttt", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						long add1 = RegisterFile.getValue(operands[1]);
						long add2 = operands[2] << 16 >> 16;
						long sum = add1 + add2;
						setFirstFlags(sum);
						FloatingPointRegisterFile.clearConditionFlag(3);
						// overflow on A+B detected when A and B have same sign and A+B has other sign.
						if ((add1 >= 0 && add2 >= 0 && sum < 0) || (add1 < 0 && add2 < 0 && sum >= 0)) {
							FloatingPointRegisterFile.setConditionFlag(2);
							throw new ProcessingException(statement, "arithmetic overflow",
									Exceptions.ARITHMETIC_OVERFLOW_EXCEPTION);
						} else
							FloatingPointRegisterFile.clearConditionFlag(2);
						RegisterFile.updateRegister(operands[0], sum);
					}
				}));
		instructionList.add(new BasicInstruction("ADDS X1,X2,X3", "Add : set X1 to (X2 plus X3) and set flags",
				BasicInstructionFormat.R_FORMAT, "000000 sssss ttttt fffff 00000 100000", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						long add1 = RegisterFile.getValue(operands[1]);
						long add2 = RegisterFile.getValue(operands[2]);
						long sum = add1 + add2;
						setFirstFlags(sum);
						FloatingPointRegisterFile.clearConditionFlag(3);
						// overflow on A+B detected when A and B have same sign and A+B has other sign.
						if ((add1 >= 0 && add2 >= 0 && sum < 0) || (add1 < 0 && add2 < 0 && sum >= 0)) {
							FloatingPointRegisterFile.setConditionFlag(2);
							throw new ProcessingException(statement, "arithmetic overflow",
									Exceptions.ARITHMETIC_OVERFLOW_EXCEPTION);
						} else
							FloatingPointRegisterFile.clearConditionFlag(2);
						RegisterFile.updateRegister(operands[0], sum);
					}
				}));
		instructionList.add(new BasicInstruction("AND X1,X2,X3", "And : Set X1 to bitwise AND of X2 and X3",
				BasicInstructionFormat.R_FORMAT, "000000 sssss ttttt fffff 00000 100100", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						RegisterFile.updateRegister(operands[0],
								RegisterFile.getValue(operands[1]) & RegisterFile.getValue(operands[2]));
					}
				}));
		instructionList.add(new BasicInstruction("ANDI X1,X2,100",
				"And Immediate : Set X1 to bitwise AND of X2 and zero-extended 16-bit immediate",
				BasicInstructionFormat.I_FORMAT, "001100 sssss fffff tttttttttttttttt", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						// ANDing with 0x0000FFFF zero-extends the immediate (high 16 bits always 0).
						RegisterFile.updateRegister(operands[0],
								RegisterFile.getValue(operands[1]) & (operands[2] & 0x0000FFFF));
					}
				}));
		instructionList.add(new BasicInstruction("ANDIS X1,X2,100",
				"And Immediate : Set X1 to bitwise AND of X2 and zero-extended 16-bit immediate and set flags",
				BasicInstructionFormat.I_FORMAT, "001100 sssss fffff tttttttttttttttt", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						long op1 = operands[1];
						long op2 = operands[2];
						// ANDing with 0x0000FFFF zero-extends the immediate (high 16 bits always 0).
						long result = RegisterFile.getValue(op1) & (op2 & 0x0000FFFF);
						setFirstFlags(result);
						FloatingPointRegisterFile.clearConditionFlag(3);
						if ((op1 >= 0 && op2 >= 0 && result < 0) || (op1 < 0 && op2 < 0 && result >= 0))
							FloatingPointRegisterFile.setConditionFlag(2);
						else
							FloatingPointRegisterFile.clearConditionFlag(2);
						RegisterFile.updateRegister(operands[0], result);
					}
				}));
		instructionList
				.add(new BasicInstruction("ANDS X1,X2,X3", "And : Set X1 to bitwise AND of X2 and X3 and set flags",
						BasicInstructionFormat.R_FORMAT, "000000 sssss ttttt fffff 00000 100100", new SimulationCode() {
							public void simulate(ProgramStatement statement) throws ProcessingException {
								long[] operands = statement.getOperands();
								long op1 = operands[1];
								long op2 = operands[2];
								long result = RegisterFile.getValue(op1) & RegisterFile.getValue(op2);
								setFirstFlags(result);
								FloatingPointRegisterFile.clearConditionFlag(3);
								if ((op1 >= 0 && op2 >= 0 && result < 0) || (op1 < 0 && op2 < 0 && result >= 0))
									FloatingPointRegisterFile.setConditionFlag(2);
								else
									FloatingPointRegisterFile.clearConditionFlag(2);
								RegisterFile.updateRegister(operands[0], result);
							}
						}));
		instructionList.add(new BasicInstruction("B target", "Branch : branch to statement at target address",
				BasicInstructionFormat.J_FORMAT, "000010 ffffffffffffffffffffffffff", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						processJump(((RegisterFile.getProgramCounter() & 0xF0000000) | (operands[0] << 2)));
					}
				}));
		instructionList.add(new BasicInstruction("B.EQ label",
				"Branch if equal : Branch to statement at label's address if flag Z=1",
				BasicInstructionFormat.I_BRANCH_FORMAT, "000100 ffffffffffffffffffffffffff", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();

						if (FloatingPointRegisterFile.getConditionFlag(1) == 1) {
							processBranch(operands[0]);
						}
					}
				}));
		instructionList.add(new BasicInstruction("B.GE label",
				"Branch if greater than or equal to (signed) : Branch to statement at label's address if flags N=V",
				BasicInstructionFormat.I_BRANCH_FORMAT, "000001 ffffffffffffffffffffffffff", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if (FloatingPointRegisterFile.getConditionFlag(0) == FloatingPointRegisterFile
								.getConditionFlag(2)) {
							processBranch(operands[0]);
						}
					}
				}));
		instructionList.add(new BasicInstruction("B.GT label",
				"Branch if greater than (signed) : Branch to statement at label's address if flags Z=0 and N=V",
				BasicInstructionFormat.I_BRANCH_FORMAT, "000111 ffffffffffffffffffffffffff", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if (FloatingPointRegisterFile.getConditionFlag(1) == 0 && FloatingPointRegisterFile
								.getConditionFlag(0) == FloatingPointRegisterFile.getConditionFlag(2)) {
							processBranch(operands[0]);
						}
					}
				}));
		instructionList.add(new BasicInstruction("B.HI label",
				"Branch if greater than (unsigned) : Branch to statement at label's address if flags Z=0 and C=1",
				BasicInstructionFormat.I_BRANCH_FORMAT, "000111 ffffffffffffffffffffffffff", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if (FloatingPointRegisterFile.getConditionFlag(1) == 0
								&& FloatingPointRegisterFile.getConditionFlag(3) == 0) {
							processBranch(operands[0]);
						}
					}
				}));
		instructionList.add(new BasicInstruction("B.HS label",
				"Branch if greater than or equal to (unsigned) : Branch to statement at label's address if flag C=1",
				BasicInstructionFormat.I_BRANCH_FORMAT, "000001 ffffffffffffffffffffffffff", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if (FloatingPointRegisterFile.getConditionFlag(3) == 1) {
							processBranch(operands[0]);
						}
					}
				}));
		instructionList.add(new BasicInstruction("B.LE label",
				"Branch if less than or equal to (signed) : Branch to statement at label's address if not (flags Z=0 and N=V)",
				BasicInstructionFormat.I_BRANCH_FORMAT, "000110 ffffffffffffffffffffffffff", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if (!(FloatingPointRegisterFile.getConditionFlag(1) == 0 && FloatingPointRegisterFile
								.getConditionFlag(0) == FloatingPointRegisterFile.getConditionFlag(2))) {
							processBranch(operands[0]);
						}
					}
				}));
		instructionList.add(new BasicInstruction("B.LO label",
				"Branch if less than (unsigned) : Branch to statement at label's address if flag C=0",
				BasicInstructionFormat.I_BRANCH_FORMAT, "000001 ffffffffffffffffffffffffff", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if (FloatingPointRegisterFile.getConditionFlag(3) == 0) {
							processBranch(operands[0]);
						}
					}
				}));
		instructionList.add(new BasicInstruction("B.LS label",
				"Branch if less than or equal to (unsigned) : Branch to statement at label's address if not (flags Z=0 and C=1)",
				BasicInstructionFormat.I_BRANCH_FORMAT, "000110 ffffffffffffffffffffffffff", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if (!(FloatingPointRegisterFile.getConditionFlag(1) == 0
								&& FloatingPointRegisterFile.getConditionFlag(3) == 1)) {
							processBranch(operands[0]);
						}
					}
				}));
		instructionList.add(new BasicInstruction("B.LT label",
				"Branch if less than (signed) : Branch to statement at label's address if flags N!=V",
				BasicInstructionFormat.I_BRANCH_FORMAT, "000001 ffffffffffffffffffffffffff", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if (FloatingPointRegisterFile.getConditionFlag(0) != FloatingPointRegisterFile
								.getConditionFlag(2)) {
							processBranch(operands[0]);
						}
					}
				}));
		instructionList.add(new BasicInstruction("B.NE label",
				"Branch if not equal : Branch to statement at label's address if flag Z=0",
				BasicInstructionFormat.I_BRANCH_FORMAT, "000101 ffffffffffffffffffffffffff", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if (FloatingPointRegisterFile.getConditionFlag(1) == 0) {
							processBranch(operands[0]);
						}
					}
				}));
		instructionList.add(new BasicInstruction("BL label",
				"Branch to statement and link : Set LR to the Program Counter and branch to statement at label's address",
				BasicInstructionFormat.I_BRANCH_FORMAT, "000001 ffffffffffffffffffffffffff", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						processReturnAddress(30);// RegisterFile.updateRegister("LR",RegisterFile.getProgramCounter());
						processBranch(operands[0]);

					}
				}));
		instructionList
				.add(new BasicInstruction("BR X1", "Branch to Register : branch to statement whose address is in X1",
						BasicInstructionFormat.R_FORMAT, "000000 fffff 00000 00000 00000 001000", new SimulationCode() {
							public void simulate(ProgramStatement statement) throws ProcessingException {
								long[] operands = statement.getOperands();
								processJump(RegisterFile.getValue(operands[0]));
							}
						}));
		instructionList.add(new BasicInstruction("CMPI X1,-100",
				"Compare immediate : Compare to immediate value and set flags", BasicInstructionFormat.I_BRANCH_FORMAT,
				"000101 fffff ssssssssssssssss 00000", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if (RegisterFile.getValue(operands[0]) - operands[1] > 0) {
							FloatingPointRegisterFile.clearConditionFlag(1);
							FloatingPointRegisterFile.clearConditionFlag(0);
						} else if ((RegisterFile.getValue(operands[0]) - operands[1] < 0)) {
							FloatingPointRegisterFile.setConditionFlag(0);
							FloatingPointRegisterFile.clearConditionFlag(1);

						} else if (RegisterFile.getValue(operands[0]) - operands[1] == 0) {
							FloatingPointRegisterFile.setConditionFlag(1);
							FloatingPointRegisterFile.clearConditionFlag(0);
						}
					}
				}));
		instructionList.add(new BasicInstruction("CMP X1,X2", "Compare : Compare register values and set flags",
				BasicInstructionFormat.I_BRANCH_FORMAT, "000101 fffff ssssssssssssssss 00000", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if ((RegisterFile.getValue(operands[0]) - RegisterFile.getValue(operands[1]) > 0)) {
							FloatingPointRegisterFile.clearConditionFlag(1);
							FloatingPointRegisterFile.clearConditionFlag(0);
						} else if ((RegisterFile.getValue(operands[0]) - RegisterFile.getValue(operands[1]) < 0)) {
							FloatingPointRegisterFile.setConditionFlag(0);
							FloatingPointRegisterFile.clearConditionFlag(1);

						} else if ((RegisterFile.getValue(operands[0]) - RegisterFile.getValue(operands[1]) == 0)) {
							FloatingPointRegisterFile.setConditionFlag(1);
							FloatingPointRegisterFile.clearConditionFlag(0);
						}
					}
				}));
		instructionList.add(new BasicInstruction("CBNZ X1,label",
				"Branch if not equal to zero : Branch to statement at label's address if X1 is not equal to zero",
				BasicInstructionFormat.I_BRANCH_FORMAT, "000101 fffff ssssssssssssssss 00000", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if (RegisterFile.getValue(operands[0]) != 0) {
							processBranch(operands[1]);
						}
					}
				}));
		instructionList.add(new BasicInstruction("CBZ X1,label",
				"Branch if equal to zero : Branch to statement at label's address if X1 is equal to zero",
				BasicInstructionFormat.I_BRANCH_FORMAT, "000100 fffff ssssssssssssssss 00000", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();

						if (RegisterFile.getValue(operands[0]) == 0) {
							processBranch(operands[1]);
						}
					}
				}));
		instructionList.add(new BasicInstruction("EOR X1,X2,X3", "Exclusive Or : Set X1 to bitwise XOR of X2 and X3",
				BasicInstructionFormat.R_FORMAT, "000000 sssss ttttt fffff 00000 100110", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						RegisterFile.updateRegister(operands[0],
								RegisterFile.getValue(operands[1]) ^ RegisterFile.getValue(operands[2]));
					}
				}));
		instructionList.add(new BasicInstruction("EORI X1,X2,100",
				"Exclusive Or Immediate : Set X1 to bitwise XOR of X2 and zero-extended 16-bit immediate",
				BasicInstructionFormat.I_FORMAT, "001110 sssss fffff tttttttttttttttt", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						// ANDing with 0x0000FFFF zero-extends the immediate (high 16 bits always 0).
						RegisterFile.updateRegister(operands[0],
								RegisterFile.getValue(operands[1]) ^ (operands[2] & 0x0000FFFF));
					}
				}));
		instructionList.add(new BasicInstruction("FCMPD S2,S4",
				"Compare equal double precision : If S2 is equal to S4 (double-precision), set Coprocessor 1 condition flag 0 true else set it false",
				BasicInstructionFormat.R_FORMAT, "010001 10001 sssss fffff 00000 110010", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if (operands[0] % 2 == 1 || operands[1] % 2 == 1) {
							throw new ProcessingException(statement, "both registers must be even-numbered");
						}
						double op1 = Double.longBitsToDouble(
								Binary.twoIntsToLong(FloatingPointRegisterFile.getValue(operands[0] + 1),
										FloatingPointRegisterFile.getValue(operands[0])));
						double op2 = Double.longBitsToDouble(
								Binary.twoIntsToLong(FloatingPointRegisterFile.getValue(operands[1] + 1),
										FloatingPointRegisterFile.getValue(operands[1])));
						if (op1 == op2) {
							FloatingPointRegisterFile.setConditionFlag(1);
							FloatingPointRegisterFile.clearConditionFlag(0);
						}

				else if (op2 > op1) {

							FloatingPointRegisterFile.setConditionFlag(0);
							FloatingPointRegisterFile.clearConditionFlag(1);
						} else if (op2 < op1) {

							FloatingPointRegisterFile.clearConditionFlag(0);
							FloatingPointRegisterFile.clearConditionFlag(0);
						}
					}
				}));
		instructionList.add(new BasicInstruction("FCMPS S0,S1",
				"Compare equal single precision : If S0 is equal to S1, set Coprocessor 1 condition flag 0 true else set it false, If S0<S1, set the negative flag",
				BasicInstructionFormat.R_FORMAT, "010001 10000 sssss fffff 00000 110010", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						double op1 = Double.longBitsToDouble(FloatingPointRegisterFile.getValue(operands[0]));
						double op2 = Double.longBitsToDouble(FloatingPointRegisterFile.getValue(operands[1]));
						if (op1 == op2) {
							FloatingPointRegisterFile.setConditionFlag(1);
							FloatingPointRegisterFile.clearConditionFlag(0);
						}

				else if (op2 > op1) {

							FloatingPointRegisterFile.setConditionFlag(0);
							FloatingPointRegisterFile.clearConditionFlag(1);
						} else if (op2 < op1) {

							FloatingPointRegisterFile.clearConditionFlag(0);
							FloatingPointRegisterFile.clearConditionFlag(0);
						}
					}
				}));
		instructionList.add(new BasicInstruction("LDA X1,label",
				"Load Register Unscaled Offset : Set X1 to sign-extended 8-bit value from effective memory byte address",
				BasicInstructionFormat.I_FORMAT, "100000 ttttt fffff ssssssssssssssss", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						RegisterFile.updateRegister(operands[0], operands[1]);

					}
				}));
		instructionList.add(new BasicInstruction("LDUR X1,[X2,-100]",
				"Load Register Unscaled Offset : Set X1 to sign-extended 8-bit value from effective memory byte address",
				BasicInstructionFormat.I_FORMAT, "100000 ttttt fffff ssssssssssssssss", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						try {
							RegisterFile.updateRegister(operands[0], Globals.memory.getByte(
									RegisterFile.getValue(operands[1]) + (operands[2] << 16 >> 16)) << 24 >> 24);
						} catch (AddressErrorException e) {
							throw new ProcessingException(statement, e);
						}
					}
				}));
		instructionList.add(new BasicInstruction("LDURB X1,[X2,-100]",
				"Load Register Unscaled Offset : Set X1 to sign-extended 8-bit value from effective memory byte address",
				BasicInstructionFormat.I_FORMAT, "100000 ttttt fffff ssssssssssssssss", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						try {
							RegisterFile.updateRegister(operands[0], Globals.memory.getByte(
									RegisterFile.getValue(operands[1]) + (operands[2] << 16 >> 16)) << 24 >> 24);
						} catch (AddressErrorException e) {
							throw new ProcessingException(statement, e);
						}
					}
				}));
		instructionList.add(// no printed reference, got opcode from SPIM
				new BasicInstruction("LDURD S2,[X1,-100]",
						"Load double word : Set S2 to 64-bit value from effective memory doubleword address",
						BasicInstructionFormat.I_FORMAT, "110101 ttttt fffff ssssssssssssssss", new SimulationCode() {
							public void simulate(ProgramStatement statement) throws ProcessingException {
								long[] operands = statement.getOperands();
								if (operands[0] % 2 == 1) {
									throw new ProcessingException(statement, "first register must be even-numbered");
								}
								// IF statement added by DPS 13-July-2011.
								if (!Globals.memory
										.doublewordAligned(RegisterFile.getValue(operands[1]) + operands[2])) {
									throw new ProcessingException(statement,
											new AddressErrorException("address not aligned on doubleword boundary ",
													Exceptions.ADDRESS_EXCEPTION_LOAD,
													RegisterFile.getValue(operands[1]) + operands[2]));
								}

								try {
									FloatingPointRegisterFile.updateRegister(operands[0],
											Globals.memory.getWord(RegisterFile.getValue(operands[1]) + operands[2]));
									FloatingPointRegisterFile.updateRegister(operands[0] + 1, Globals.memory
											.getWord(RegisterFile.getValue(operands[1]) + operands[2] + 4));
								} catch (AddressErrorException e) {
									throw new ProcessingException(statement, e);
								}
							}
						}));
		instructionList.add(// no printed reference, got opcode from SPIM
				new BasicInstruction("LDURS S1,[X1,-100]",
						"Load double word : Set S2 to 64-bit value from effective memory doubleword address",
						BasicInstructionFormat.I_FORMAT, "110101 ttttt fffff ssssssssssssssss", new SimulationCode() {
							public void simulate(ProgramStatement statement) throws ProcessingException {
								long[] operands = statement.getOperands();
								if (operands[0] % 2 == 1) {
									throw new ProcessingException(statement, "first register must be even-numbered");
								}
								// IF statement added by DPS 13-July-2011.
								if (!Globals.memory
										.doublewordAligned(RegisterFile.getValue(operands[1]) + operands[2])) {
									throw new ProcessingException(statement,
											new AddressErrorException("address not aligned on doubleword boundary ",
													Exceptions.ADDRESS_EXCEPTION_LOAD,
													RegisterFile.getValue(operands[1]) + operands[2]));
								}

								try {
									FloatingPointRegisterFile.updateRegister(operands[0],
											Globals.memory.getWord(RegisterFile.getValue(operands[1]) + operands[2]));

								} catch (AddressErrorException e) {
									throw new ProcessingException(statement, e);
								}
							}
						}));
		instructionList.add(new BasicInstruction("LDURSW X1,[X2,-100]",
				"Load word : Set X1 to contents of effective memory word address", BasicInstructionFormat.I_FORMAT,
				"100011 ttttt fffff ssssssssssssssss", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						try {
							RegisterFile.updateRegister(operands[0],
									Globals.memory.getWord(RegisterFile.getValue(operands[1]) + operands[2]));
						} catch (AddressErrorException e) {
							throw new ProcessingException(statement, e);
						}
					}
				}));
		instructionList.add(new BasicInstruction("LSL X1,X2,10",
				"Shift left logical : Set X1 to result of shifting X2 left by number of bits specified by immediate",
				BasicInstructionFormat.R_FORMAT, "000000 00000 sssss fffff ttttt 000000", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						RegisterFile.updateRegister(operands[0], RegisterFile.getValue(operands[1]) << operands[2]);
					}
				}));
		instructionList.add(new BasicInstruction("LSR X1,X2,10",
				"Shift right logical : Set X1 to result of shifting X2 right by number of bits specified by immediate",
				BasicInstructionFormat.R_FORMAT, "000000 00000 sssss fffff ttttt 000010", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						// must zero-fill, so use ">>>" instead of ">>".
						RegisterFile.updateRegister(operands[0], RegisterFile.getValue(operands[1]) >>> operands[2]);
					}
				}));
		instructionList.add(new BasicInstruction("MOV X1,X2", "Move : Set X1 to the value in X2",
				BasicInstructionFormat.R_FORMAT, "000000 sssss fffff 00000 00000 100101", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						RegisterFile.updateRegister(operands[0],
								RegisterFile.getValue(operands[1]) | RegisterFile.getValue(31));
					}
				}));
		instructionList
				.add(new BasicInstruction("MOVZ X1,1,-100", "Move : Set X1 to the value in X2 shifted by 16 or 0",
						BasicInstructionFormat.R_FORMAT, "0000 fffff s tttttttttttttttt 100101", new SimulationCode() {

							public void simulate(ProgramStatement statement) throws ProcessingException {
								long[] operands = statement.getOperands();
								if (operands[1] % 16 != 0) {
									throw new ProcessingException(statement, "shift amount must be 0 or 16");
								}
								RegisterFile.updateRegister(operands[0], operands[2] << operands[1]);
							}
						}));
		instructionList.add(new BasicInstruction("MOVK X1,1,-100", "Move : Set X1 to the value in X2",
				BasicInstructionFormat.R_FORMAT, "0000 fffff s tttttttttttttttt 100101", new SimulationCode() {

					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if (operands[1] % 16 != 0) {
							throw new ProcessingException(statement, "shift amount must be 0 or 16");
						}
						int bitlength = (int) Math.ceil(Math.log(operands[2]) / Math.log(2));
						long result = (RegisterFile.getValue(operands[0]) >> bitlength << bitlength) + operands[2];
						RegisterFile.updateRegister(operands[0], result);
					}
				}));
		instructionList.add(new BasicInstruction("MUL X1,X2,X3", "Multiply : set X1 to (X2 multiplied by X3)",
				BasicInstructionFormat.R_FORMAT, "000000 sssss ttttt fffff 00000 100000", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						long mul1 = RegisterFile.getValue(operands[1]);
						long mul2 = RegisterFile.getValue(operands[2]);
						long quot = mul1 * mul2;
						// overflow on A+B detected when A and B have same sign and A+B has other sign.
						if ((mul1 >= 0 && mul2 < 0 && quot < 0) || (mul1 < 0 && mul2 >= 0 && quot >= 0)) {
							throw new ProcessingException(statement, "arithmetic overflow",
									Exceptions.ARITHMETIC_OVERFLOW_EXCEPTION);
						}
						RegisterFile.updateRegister(operands[0], quot);
					}
				}));
		instructionList.add(new BasicInstruction("ORR X1,X2,X3", "Inclusive Or : Set X1 to bitwise OR of X2 and X3",
				BasicInstructionFormat.R_FORMAT, "000000 sssss ttttt fffff 00000 100101", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						RegisterFile.updateRegister(operands[0],
								RegisterFile.getValue(operands[1]) | RegisterFile.getValue(operands[2]));
					}
				}));
		instructionList.add(new BasicInstruction("ORRI X1,X2,100",
				"Inclusive Or Immediate : Set X1 to bitwise OR of X2 and zero-extended 16-bit immediate",
				BasicInstructionFormat.I_FORMAT, "001101 sssss fffff tttttttttttttttt", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						// ANDing with 0x0000FFFF zero-extends the immediate (high 16 bits always 0).
						RegisterFile.updateRegister(operands[0],
								RegisterFile.getValue(operands[1]) | (operands[2] & 0x0000FFFF));
					}
				}));
		instructionList.add(new BasicInstruction("STUR X1,[X2,-100]",
				"Store Byte Unscaled Offset : Store the low-order 8 bits of X1 into the effective memory byte address",
				BasicInstructionFormat.I_FORMAT, "101000 ttttt fffff ssssssssssssssss", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						try {
							Globals.memory.setByte(RegisterFile.getValue(operands[1]) + (operands[2] << 16 >> 16),
									RegisterFile.getValue(operands[0]));
						} catch (AddressErrorException e) {
							throw new ProcessingException(statement, e);
						}
					}
				}));
		instructionList.add(new BasicInstruction("STURB X1,[X2,-100]",
				"Store Byte Unscaled Offset : Store the low-order 8 bits of X1 into the effective memory byte address",
				BasicInstructionFormat.I_FORMAT, "101000 ttttt fffff ssssssssssssssss", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						try {
							Globals.memory.setByte(RegisterFile.getValue(operands[1]) + (operands[2] << 16 >> 16),
									RegisterFile.getValue(operands[0]) & 0x000000ff);
						} catch (AddressErrorException e) {
							throw new ProcessingException(statement, e);
						}
					}
				}));
		instructionList.add(new BasicInstruction("STURH X1,[X2,-100]",
				"Store halfword : Store the low-order 16 bits of X1 into the effective memory halfword address",
				BasicInstructionFormat.I_FORMAT, "101001 ttttt fffff ssssssssssssssss", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						try {
							Globals.memory.setHalf(RegisterFile.getValue(operands[1]) + (operands[2] << 16 >> 16),
									RegisterFile.getValue(operands[0]) & 0x0000ffff);
						} catch (AddressErrorException e) {
							throw new ProcessingException(statement, e);
						}
					}
				}));
		instructionList.add(new BasicInstruction("STURW X1,[X2,-100]",
				"Store word : Store contents of X1 into effective memory word address", BasicInstructionFormat.I_FORMAT,
				"101011 ttttt fffff ssssssssssssssss", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						try {
							Globals.memory.setWord(RegisterFile.getValue(operands[1]) + operands[2],
									RegisterFile.getValue(operands[0]));
						} catch (AddressErrorException e) {
							throw new ProcessingException(statement, e);
						}
					}
				}));
		instructionList.add(new BasicInstruction("SUB X1,X2,X3", "Subtract : set X1 to (X2 minus X3)",
				BasicInstructionFormat.R_FORMAT, "000000 sssss ttttt fffff 00000 100010", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						long sub1 = RegisterFile.getValue(operands[1]);
						long sub2 = RegisterFile.getValue(operands[2]);
						long dif = sub1 - sub2;
						// overflow on A-B detected when A and B have opposite signs and A-B has B's
						// sign
						if ((sub1 >= 0 && sub2 < 0 && dif < 0) || (sub1 < 0 && sub2 >= 0 && dif >= 0)) {
							throw new ProcessingException(statement, "arithmetic overflow",
									Exceptions.ARITHMETIC_OVERFLOW_EXCEPTION);
						}
						RegisterFile.updateRegister(operands[0], dif);
					}
				}));
		instructionList.add(new BasicInstruction("SUBI X1,X2,-100",
				"Subtract Immediate : set X1 to (X2 minus signed 16-bit immediate)", BasicInstructionFormat.I_FORMAT,
				"001000 sssss fffff tttttttttttttttt", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						long sub1 = RegisterFile.getValue(operands[1]);
						long sub2 = operands[2] << 16 >> 16;
						long dif = sub1 - sub2;
						// overflow on A+B detected when A and B have same sign and A+B has other sign.
						if ((sub1 >= 0 && sub2 < 0 && dif < 0) || (sub1 < 0 && sub2 >= 0 && dif >= 0)) {
							throw new ProcessingException(statement, "arithmetic overflow",
									Exceptions.ARITHMETIC_OVERFLOW_EXCEPTION);
						}
						RegisterFile.updateRegister(operands[0], dif);
					}
				}));
		instructionList.add(new BasicInstruction("SUBIS X1,X2,-100",
				"Subtract Immediate : set X1 to (X2 minus signed 16-bit immediate)", BasicInstructionFormat.I_FORMAT,
				"001000 sssss fffff tttttttttttttttt", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						long sub1 = RegisterFile.getValue(operands[1]);
						long sub2 = operands[2] << 16 >> 16;
						long dif = sub1 - sub2;
						setFirstFlags(dif);
						FloatingPointRegisterFile.clearConditionFlag(3);
						// overflow on A+B detected when A and B have same sign and A+B has other sign.
						if ((sub1 >= 0 && sub2 < 0 && dif < 0) || (sub1 < 0 && sub2 >= 0 && dif >= 0)) {
							FloatingPointRegisterFile.setConditionFlag(2);
							throw new ProcessingException(statement, "arithmetic overflow",
									Exceptions.ARITHMETIC_OVERFLOW_EXCEPTION);
						} else
							FloatingPointRegisterFile.clearConditionFlag(2);
						RegisterFile.updateRegister(operands[0], dif);
					}
				}));
		instructionList.add(new BasicInstruction("SUBS X1,X2,X3", "Subtract : set X1 to (X2 minus X3)",
				BasicInstructionFormat.R_FORMAT, "000000 sssss ttttt fffff 00000 100010", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						long sub1 = RegisterFile.getValue(operands[1]);
						long sub2 = RegisterFile.getValue(operands[2]);
						long dif = sub1 - sub2;
						setFirstFlags(dif);
						FloatingPointRegisterFile.clearConditionFlag(3);
						// overflow on A-B detected when A and B have opposite signs and A-B has B's
						// sign
						if ((sub1 >= 0 && sub2 < 0 && dif < 0) || (sub1 < 0 && sub2 >= 0 && dif >= 0)) {
							FloatingPointRegisterFile.setConditionFlag(2);
							throw new ProcessingException(statement, "arithmetic overflow",
									Exceptions.ARITHMETIC_OVERFLOW_EXCEPTION);
						} else
							FloatingPointRegisterFile.clearConditionFlag(2);
						RegisterFile.updateRegister(operands[0], dif);
					}
				}));
		instructionList.add(new BasicInstruction("SVC",
				"Issue a service call : Execute the service call specified by value in X0",
				BasicInstructionFormat.R_FORMAT, "000000 00000 00000 00000 00000 001100", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						findAndSimulateSyscall(RegisterFile.getValue(0), statement);
					}
				}));

		/////////////////////// Floating Point Instructions Start Here ////////////////
		instructionList.add(new BasicInstruction("FADDD S2,S4,S6",
				"Floating point addition double precision : Set S2 to double-precision floating point value of S4 plus S6",
				BasicInstructionFormat.R_FORMAT, "010001 10001 ttttt sssss fffff 000000", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
//						if (operands[0] % 2 == 1 || operands[1] % 2 == 1 || operands[2] % 2 == 1) {
//							throw new ProcessingException(statement, "all registers must be even-numbered");
//						}
						double add1 = Double.longBitsToDouble(
								Binary.twoIntsToLong(FloatingPointRegisterFile.getValue(operands[1] + 1),
										FloatingPointRegisterFile.getValue(operands[1])));
						double add2 = Double.longBitsToDouble(
								Binary.twoIntsToLong(FloatingPointRegisterFile.getValue(operands[2] + 1),
										FloatingPointRegisterFile.getValue(operands[2])));
						double sum = add1 + add2;
						long longSum = Double.doubleToLongBits(sum);
						FloatingPointRegisterFile.updateRegister(operands[0] + 1, Binary.highOrderLongToInt(longSum));
						FloatingPointRegisterFile.updateRegister(operands[0], Binary.lowOrderLongToInt(longSum));
					}
				}));
		instructionList.add(new BasicInstruction("FADDS S0,S1,S3",
				"Floating point addition single precision : Set S0 to single-precision floating point value of S1 plus S3",
				BasicInstructionFormat.R_FORMAT, "010001 10000 ttttt sssss fffff 000000", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						double add1 = Double.longBitsToDouble(FloatingPointRegisterFile.getValue(operands[1]));
						double add2 = Double.longBitsToDouble(FloatingPointRegisterFile.getValue(operands[2]));
						double sum = add1 + add2;
						// overflow detected when sum is positive or negative infinity.
						/*
						 * if (sum == Float.NEGATIVE_INFINITY || sum == Float.POSITIVE_INFINITY) { throw
						 * new ProcessingException(statement,"arithmetic overflow"); }
						 */
						FloatingPointRegisterFile.updateRegister(operands[0], Double.doubleToLongBits(sum));
					}
				}));
		instructionList.add(new BasicInstruction("FDIVD S2,S4,S6",
				"Floating point division double precision : Set S2 to double-precision floating point value of S4 divided by S6",
				BasicInstructionFormat.R_FORMAT, "010001 10001 ttttt sssss fffff 000011", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
//						if (operands[0] % 2 == 1 || operands[1] % 2 == 1 || operands[2] % 2 == 1) {
//							throw new ProcessingException(statement, "all registers must be even-numbered");
//						}
						double div1 = Double.longBitsToDouble(
								Binary.twoIntsToLong(FloatingPointRegisterFile.getValue(operands[1] + 1),
										FloatingPointRegisterFile.getValue(operands[1])));
						double div2 = Double.longBitsToDouble(
								Binary.twoIntsToLong(FloatingPointRegisterFile.getValue(operands[2] + 1),
										FloatingPointRegisterFile.getValue(operands[2])));
						double quot = div1 / div2;
						long longQuot = Double.doubleToLongBits(quot);
						FloatingPointRegisterFile.updateRegister(operands[0] + 1, Binary.highOrderLongToInt(longQuot));
						FloatingPointRegisterFile.updateRegister(operands[0], Binary.lowOrderLongToInt(longQuot));
					}
				}));
		instructionList.add(new BasicInstruction("FDIVS S0,S1,S3",
				"Floating point division single precision : Set S0 to single-precision floating point value of S1 divided by S3",
				BasicInstructionFormat.R_FORMAT, "010001 10000 ttttt sssss fffff 000011", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						double div1 = Double.longBitsToDouble(FloatingPointRegisterFile.getValue(operands[1]));
						double div2 = Double.longBitsToDouble(FloatingPointRegisterFile.getValue(operands[2]));
						double quot = div1 / div2;
						FloatingPointRegisterFile.updateRegister(operands[0], Double.doubleToLongBits(quot));
					}
				}));
		instructionList.add(new BasicInstruction("FMULD S2,S4,S6",
				"Floating point multiplication double precision : Set S2 to double-precision floating point value of S4 times S6",
				BasicInstructionFormat.R_FORMAT, "010001 10001 ttttt sssss fffff 000010", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
//						if (operands[0] % 2 == 1 || operands[1] % 2 == 1 || operands[2] % 2 == 1) {
//							throw new ProcessingException(statement, "all registers must be even-numbered");
//						}
						double mul1 = Double.longBitsToDouble(
								Binary.twoIntsToLong(FloatingPointRegisterFile.getValue(operands[1] + 1),
										FloatingPointRegisterFile.getValue(operands[1])));
						double mul2 = Double.longBitsToDouble(
								Binary.twoIntsToLong(FloatingPointRegisterFile.getValue(operands[2] + 1),
										FloatingPointRegisterFile.getValue(operands[2])));
						double prod = mul1 * mul2;
						long longProd = Double.doubleToLongBits(prod);
						FloatingPointRegisterFile.updateRegister(operands[0] + 1, Binary.highOrderLongToInt(longProd));
						FloatingPointRegisterFile.updateRegister(operands[0], Binary.lowOrderLongToInt(longProd));
					}
				}));
		instructionList.add(new BasicInstruction("FMULS S0,S1,S3",
				"Floating point multiplication single precision : Set S0 to single-precision floating point value of S1 times S3",
				BasicInstructionFormat.R_FORMAT, "010001 10000 ttttt sssss fffff 000010", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						double mul1 = Double.longBitsToDouble(FloatingPointRegisterFile.getValue(operands[1]));
						double mul2 = Double.longBitsToDouble(FloatingPointRegisterFile.getValue(operands[2]));
						double prod = mul1 * mul2;
						FloatingPointRegisterFile.updateRegister(operands[0], Double.doubleToLongBits(prod));
					}
				}));
		instructionList.add(new BasicInstruction("FSUBD S2,S4,S6",
				"Floating point subtraction double precision : Set S2 to double-precision floating point value of S4 minus S6",
				BasicInstructionFormat.R_FORMAT, "010001 10001 ttttt sssss fffff 000001", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
//						if (operands[0] % 2 == 1 || operands[1] % 2 == 1 || operands[2] % 2 == 1) {
//							throw new ProcessingException(statement, "all registers must be even-numbered");
//						}
						double sub1 = Double.longBitsToDouble(
								Binary.twoIntsToLong(FloatingPointRegisterFile.getValue(operands[1] + 1),
										FloatingPointRegisterFile.getValue(operands[1])));
						double sub2 = Double.longBitsToDouble(
								Binary.twoIntsToLong(FloatingPointRegisterFile.getValue(operands[2] + 1),
										FloatingPointRegisterFile.getValue(operands[2])));
						double diff = sub1 - sub2;
						long longDiff = Double.doubleToLongBits(diff);
						FloatingPointRegisterFile.updateRegister(operands[0] + 1, Binary.highOrderLongToInt(longDiff));
						FloatingPointRegisterFile.updateRegister(operands[0], Binary.lowOrderLongToInt(longDiff));
					}
				}));
		instructionList.add(new BasicInstruction("FSUBS S0,S1,S3",
				"Floating point subtraction single precision : Set S0 to single-precision floating point value of S1  minus S3",
				BasicInstructionFormat.R_FORMAT, "010001 10000 ttttt sssss fffff 000001", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						double sub1 = Double.doubleToLongBits(FloatingPointRegisterFile.getValue(operands[1]));
						double sub2 = Double.doubleToLongBits(FloatingPointRegisterFile.getValue(operands[2]));
						double diff = sub1 - sub2;
						FloatingPointRegisterFile.updateRegister(operands[0], Double.doubleToLongBits(diff));
					}
				}));
		instructionList.add(new BasicInstruction("SDIV X1,X2,X3",
				"Division with overflow : Divide X2 by X3 then set X1 to quotient", BasicInstructionFormat.R_FORMAT,
				"000000 fffff sssss ttttt 00000 011010", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if (RegisterFile.getValue(operands[2]) == 0) {
							// Note: no exceptions and undefined results for zero div
							// COD3 Appendix A says "with overflow" but MIPS 32 instruction set
							// specification says "no arithmetic exception under any circumstances".
							return;
						}
						RegisterFile.updateRegister(operands[0],
								RegisterFile.getValue(operands[1]) / RegisterFile.getValue(operands[2]));
					}
				}));
		instructionList.add(new BasicInstruction("SMULH X1,X2,X3",
				"Multiplication without overflow  : Set HI to high-order 32 bits and X1 to product of X2 and X3 ",
				BasicInstructionFormat.R_FORMAT, "011100 sssss ttttt fffff 00000 000010", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						long product = (long) RegisterFile.getValue(operands[1])
								* (long) RegisterFile.getValue(operands[2]);
						RegisterFile.updateRegister(33, (int) (product >> 32));
						RegisterFile.updateRegister(operands[0], (int) (product >> 32));

					}
				}));
		instructionList.add( // no printed reference, got opcode from SPIM
				new BasicInstruction("STURD S2,[X1,-100]",
						"Store double word from Coprocessor 1 (FPU)) : Store 64 bit value in S2 to effective memory doubleword address",
						BasicInstructionFormat.I_FORMAT, "111101 ttttt fffff ssssssssssssssss", new SimulationCode() {
							public void simulate(ProgramStatement statement) throws ProcessingException {
								long[] operands = statement.getOperands();
//								if (operands[0] % 2 == 1) {
//									throw new ProcessingException(statement, "first register must be even-numbered");
//								}
								// IF statement added by DPS 13-July-2011.
								if (!Globals.memory
										.doublewordAligned(RegisterFile.getValue(operands[1]) + operands[2])) {
									throw new ProcessingException(statement,
											new AddressErrorException("address not aligned on doubleword boundary ",
													Exceptions.ADDRESS_EXCEPTION_STORE,
													RegisterFile.getValue(operands[2]) + operands[1]));
								}
								try {
									Globals.memory.setWord(RegisterFile.getValue(operands[1]) + operands[2],
											FloatingPointRegisterFile.getValue(operands[0]));
									Globals.memory.setWord(RegisterFile.getValue(operands[1]) + operands[2] + 4,
											FloatingPointRegisterFile.getValue(operands[0] + 1));
								} catch (AddressErrorException e) {
									throw new ProcessingException(statement, e);
								}
							}
						}));
		instructionList.add(new BasicInstruction("STURS S0,[X1,-100]",
				"Store word from Coprocesor 1 (FPU) : Store 32 bit value in S1 to effective memory word address",
				BasicInstructionFormat.I_FORMAT, "111001 ttttt fffff ssssssssssssssss", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						try {
							Globals.memory.setWord(RegisterFile.getValue(operands[1]) + operands[2],
									FloatingPointRegisterFile.getValue(operands[0]));
						} catch (AddressErrorException e) {
							throw new ProcessingException(statement, e);
						}
					}
				}));
		instructionList.add(new BasicInstruction("UDIV X1,X2,X3",
				"Unsigned divide : Divide unsigned X2 by X3 then set X1 to quotient", BasicInstructionFormat.R_FORMAT,
				"000000 fffff sssss ttttt 00000 011011", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						if (RegisterFile.getValue(operands[2]) == 0) {
							// Note: no exceptions, and undefined results for zero divide
							return;
						}
						long oper1 = ((long) RegisterFile.getValue(operands[1])) << 32 >>> 32;
						long oper2 = ((long) RegisterFile.getValue(operands[2])) << 32 >>> 32;
						RegisterFile.updateRegister(operands[0], (int) (((oper1 / oper2) << 32) >> 32));
					}
				}));
		instructionList.add(new BasicInstruction("UMULH X1,X2,X3",
				"Multiplication unsigned : Set HI to high-order 32 bits and set X1 to product of X2 and X3",
				BasicInstructionFormat.R_FORMAT, "000000 fffff sssss ttttt 00000 011001", new SimulationCode() {
					public void simulate(ProgramStatement statement) throws ProcessingException {
						long[] operands = statement.getOperands();
						long product = (((long) RegisterFile.getValue(operands[1])) << 32 >>> 32)
								* (((long) RegisterFile.getValue(operands[2])) << 32 >>> 32);
						// Register 33 is HIGH
						RegisterFile.updateRegister(33, (int) (product >> 32));
						RegisterFile.updateRegister(operands[0], (int) (product >> 32));

					}
				}));

		////////////// READ PSEUDO-INSTRUCTION SPECS FROM DATA FILE AND ADD
		////////////// //////////////////////
		addPseudoInstructions();

		////////////// GET AND CREATE LIST OF SYSCALL FUNCTION OBJECTS
		////////////// ////////////////////
		syscallLoader = new SyscallLoader();
		syscallLoader.loadSyscalls();

		// Initialization step. Create token list for each instruction example. This is
		// used by parser to determine user program correct syntax.
		for (int i = 0; i < instructionList.size(); i++) {
			Instruction inst = (Instruction) instructionList.get(i);
			inst.createExampleTokenList();
		}

		HashMap maskMap = new HashMap();
		ArrayList matchMaps = new ArrayList();
		for (int i = 0; i < instructionList.size(); i++) {
			Object rawInstr = instructionList.get(i);
			if (rawInstr instanceof BasicInstruction) {
				BasicInstruction basic = (BasicInstruction) rawInstr;
				Integer mask = Integer.valueOf(basic.getOpcodeMask());
				Integer match = Integer.valueOf(basic.getOpcodeMatch());
				HashMap matchMap = (HashMap) maskMap.get(mask);
				if (matchMap == null) {
					matchMap = new HashMap();
					maskMap.put(mask, matchMap);
					matchMaps.add(new MatchMap(mask, matchMap));
				}
				matchMap.put(match, basic);
			}
		}
		Collections.sort(matchMaps);
		this.opcodeMatchMaps = matchMaps;
	}

	public BasicInstruction findByBinaryCode(long binaryInstr) {
		ArrayList matchMaps = this.opcodeMatchMaps;
		for (int i = 0; i < matchMaps.size(); i++) {
			MatchMap map = (MatchMap) matchMaps.get(i);
			BasicInstruction ret = map.find(binaryInstr);
			if (ret != null)
				return ret;
		}
		return null;
	}

	/*
	 * METHOD TO ADD PSEUDO-INSTRUCTIONS
	 */

	private void addPseudoInstructions() {
		InputStream is = null;
		BufferedReader in = null;
		try {
			// leading "/" prevents package name being prepended to filepath.
			is = this.getClass().getResourceAsStream("/PseudoOps.txt");
			in = new BufferedReader(new InputStreamReader(is));
		} catch (NullPointerException e) {
			System.out.println("Error: MIPS pseudo-instruction file PseudoOps.txt not found.");
			System.exit(0);
		}
		try {
			String line, pseudoOp, template, firstTemplate, token;
			String description;
			StringTokenizer tokenizer;
			while ((line = in.readLine()) != null) {
				// skip over: comment lines, empty lines, lines starting with blank.
				if (!line.startsWith("#") && !line.startsWith(" ") && line.length() > 0) {
					description = "";
					tokenizer = new StringTokenizer(line, "\t");
					pseudoOp = tokenizer.nextToken();
					template = "";
					firstTemplate = null;
					while (tokenizer.hasMoreTokens()) {
						token = tokenizer.nextToken();
						if (token.startsWith("#")) {
							// Optional description must be last token in the line.
							description = token.substring(1);
							break;
						}
						if (token.startsWith("COMPACT")) {
							// has second template for Compact (16-bit) memory config -- added DPS 3 Aug
							// 2009
							firstTemplate = template;
							template = "";
							continue;
						}
						template = template + token;
						if (tokenizer.hasMoreTokens()) {
							template = template + "\n";
						}
					}
					ExtendedInstruction inst = (firstTemplate == null)
							? new ExtendedInstruction(pseudoOp, template, description)
							: new ExtendedInstruction(pseudoOp, firstTemplate, template, description);
					instructionList.add(inst);
					// if (firstTemplate != null) System.out.println("\npseudoOp:
					// "+pseudoOp+"\ndefault template:\n"+firstTemplate+"\ncompact
					// template:\n"+template);
				}
			}
			in.close();
		} catch (IOException ioe) {
			System.out.println("Internal Error: MIPS pseudo-instructions could not be loaded.");
			System.exit(0);
		} catch (Exception ioe) {
			System.out.println("Error: Invalid MIPS pseudo-instruction specification.");
			System.exit(0);
		}

	}

	/**
	 * Given an operator mnemonic, will return the corresponding Instruction
	 * object(s) from the instruction set. Uses straight linear search technique.
	 * 
	 * @param name
	 *            operator mnemonic (e.g. addi, sw,...)
	 * @return list of corresponding Instruction object(s), or null if not found.
	 */
	public ArrayList matchOperator(String name) {
		ArrayList matchingInstructions = null;
		// Linear search for now....
		for (int i = 0; i < instructionList.size(); i++) {
			if (((Instruction) instructionList.get(i)).getName().equalsIgnoreCase(name)) {
				if (matchingInstructions == null)
					matchingInstructions = new ArrayList();
				matchingInstructions.add(instructionList.get(i));
			}
		}
		return matchingInstructions;
	}

	/**
	 * Given a string, will return the Instruction object(s) from the instruction
	 * set whose operator mnemonic prefix matches it. Case-insensitive. For example
	 * "s" will match "sw", "sh", "sb", etc. Uses straight linear search technique.
	 * 
	 * @param name
	 *            a string
	 * @return list of matching Instruction object(s), or null if none match.
	 */
	public ArrayList prefixMatchOperator(String name) {
		ArrayList matchingInstructions = null;
		// Linear search for now....
		if (name != null) {
			for (int i = 0; i < instructionList.size(); i++) {
				if (((Instruction) instructionList.get(i)).getName().toLowerCase().startsWith(name.toLowerCase())) {
					if (matchingInstructions == null)
						matchingInstructions = new ArrayList();
					matchingInstructions.add(instructionList.get(i));
				}
			}
		}
		return matchingInstructions;
	}

	/*
	 * Method to find and invoke a syscall given its service number. Each syscall
	 * function is represented by an object in an array list. Each object is of a
	 * class that implements Syscall or extends AbstractSyscall.
	 */

	private void findAndSimulateSyscall(long number, ProgramStatement statement) throws ProcessingException {
		Syscall service = syscallLoader.findSyscall(number);
		if (service != null) {
			service.simulate(statement);
			return;
		}
		throw new ProcessingException(statement, "invalid or unimplemented syscall service: " + number + " ",
				Exceptions.SYSCALL_EXCEPTION);
	}

	/*
	 * Method to process a successful branch condition. DO NOT USE WITH JUMP
	 * INSTRUCTIONS! The branch operand is a relative displacement in words whereas
	 * the jump operand is an absolute address in bytes.
	 *
	 * The parameter is displacement operand from instruction.
	 *
	 * Handles delayed branching if that setting is enabled.
	 */
	// 4 January 2008 DPS: The subtraction of 4 bytes (instruction length) after
	// the shift has been removed. It is left in as commented-out code below.
	// This has the effect of always branching as if delayed branching is enabled,
	// even if it isn't. This mod must work in conjunction with
	// ProgramStatement.java, buildBasicStatementFromBasicInstruction() method near
	// the bottom (currently line 194, heavily commented).

	private void processBranch(long displacement) {
		if (Globals.getSettings().getDelayedBranchingEnabled()) {
			// Register the branch target address (absolute byte address).
			DelayedBranch.register(RegisterFile.getProgramCounter() + (displacement << 2));
		} else {
			// Decrement needed because PC has already been incremented
			RegisterFile.setProgramCounter(RegisterFile.getProgramCounter() + (displacement << 2)); // -
																									// Instruction.INSTRUCTION_LENGTH);
		}
	}

	/*
	 * Method to process a jump. DO NOT USE WITH BRANCH INSTRUCTIONS! The branch
	 * operand is a relative displacement in words whereas the jump operand is an
	 * absolute address in bytes.
	 *
	 * The parameter is jump target absolute byte address.
	 *
	 * Handles delayed branching if that setting is enabled.
	 */

	private void processJump(long targetAddress) {
		if (Globals.getSettings().getDelayedBranchingEnabled()) {
			DelayedBranch.register(targetAddress);
		} else {
			RegisterFile.setProgramCounter(targetAddress);
		}
	}

	/*
	 * Method to process storing of a return address in the given register. This is
	 * used only by the "and link" instructions: jal, jalr, bltzal, bgezal. If
	 * delayed branching setting is off, the return address is the address of the
	 * next instruction (e.g. the current PC value). If on, the return address is
	 * the instruction following that, to skip over the delay slot.
	 *
	 * The parameter is register number to receive the return address.
	 */

	private void processReturnAddress(int register) {
		RegisterFile.updateRegister(register, RegisterFile.getProgramCounter()
				+ ((Globals.getSettings().getDelayedBranchingEnabled()) ? Instruction.INSTRUCTION_LENGTH : 0));
	}

	private void setFirstFlags(long result) {
		if (result < 0)
			FloatingPointRegisterFile.setConditionFlag(0);
		else
			FloatingPointRegisterFile.clearConditionFlag(0);
		if (result == 0)
			FloatingPointRegisterFile.setConditionFlag(1);
		else
			FloatingPointRegisterFile.clearConditionFlag(1);
	}

	private static class MatchMap implements Comparable {
		private int mask;
		private int maskLength; // number of 1 bits in mask
		private HashMap matchMap;

		public MatchMap(int mask, HashMap matchMap) {
			this.mask = mask;
			this.matchMap = matchMap;

			int k = 0;
			int n = mask;
			while (n != 0) {
				k++;
				n &= n - 1;
			}
			this.maskLength = k;
		}

		public boolean equals(Object o) {
			return o instanceof MatchMap && mask == ((MatchMap) o).mask;
		}

		public int compareTo(Object other) {
			MatchMap o = (MatchMap) other;
			int d = o.maskLength - this.maskLength;
			if (d == 0)
				d = this.mask - o.mask;
			return d;
		}

		public BasicInstruction find(long instr) {
			long match = Long.valueOf(instr & mask);
			return (BasicInstruction) matchMap.get(match);
		}
	}
}
