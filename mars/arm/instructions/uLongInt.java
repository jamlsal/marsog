package mars.arm.instructions;
import java.io.*;
import java.lang.Math;
import java.math.BigInteger;
import java.util.*;
//creates unsigned long integer for 64 bit registers
/*
USAGE:
      long num = XXXXXXX;
      className number = new className(); //class object
      long value = number.unsignedInt(num);//use method from uLongInt.java
      System.out.print("Unsigned value of input: ");
      System.out.println(Long.toUnsignedString(value));

*/
public class uLongInt {

    public long unsignedInt(long number) {

        ////convert from bigint to unsigned long
        BigInteger bigInteger = BigInteger.valueOf(Long.MAX_VALUE)
                .add(BigInteger.valueOf(number));
        long uInt = bigInteger.longValue();
        return uInt;
    }


}
